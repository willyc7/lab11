﻿using System;

namespace Cards
{
    class Deck
    {
        private Card[] cards;

        public Deck()
        { 
        }

        public void Initialize()
        {
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */
            this.cards = new Card[ItalianSeed.GetNames(typeof(ItalianSeed)).Length * ItalianValue.GetValues(typeof(ItalianValue)).Length];
            int i = 0;
            foreach(ItalianSeed seed in (ItalianSeed[]) ItalianSeed.GetValues(typeof(ItalianSeed)))
            {
                foreach(ItalianValue value in (ItalianValue[]) ItalianValue.GetValues(typeof(ItalianValue)))
                {
                    this.cards[i] = new Card(value.ToString(), seed.ToString());
                    i++;
                }
            }
        }

        public void Print()
        {
            foreach(Card card in cards)
            {
                Console.WriteLine(card.ToString() + "\n");
            }
        }

        public Card this[ItalianSeed seed, ItalianValue val]
        {
            get 
            {
                return this.cards[(int)seed * ItalianValue.GetValues(typeof(ItalianValue)).Length + (int)val]; 
            }
        }

    }

    enum ItalianSeed
    {
        DENARI,
        COPPE,
        SPADE,
        BASTONI
    }

    enum ItalianValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE
    }
}
