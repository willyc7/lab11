﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    class FrenchDeck
    {
        private Card[] cards;

        public FrenchDeck()
        {
        }

        public void Initialize()
        {

            this.cards = new Card[FrenchSeed.GetNames(typeof(FrenchSeed)).Length * FrenchValue.GetValues(typeof(FrenchValue)).Length];
            int i = 0;
            foreach (FrenchSeed seed in (FrenchSeed[])FrenchSeed.GetValues(typeof(FrenchSeed)))
            {
                foreach (FrenchValue value in (FrenchValue[])FrenchValue.GetValues(typeof(FrenchValue)))
                {
                    this.cards[i] = new Card(value.ToString(), seed.ToString());
                    i++;
                }
            }
        }

        public void Print()
        {
            foreach (Card card in cards)
            {
                Console.WriteLine(card.ToString() + "\n");
            }
        }

        public Card this[FrenchSeed seed, FrenchValue val]
        {
            get
            {
                return this.cards[(int)seed * FrenchValue.GetValues(typeof(FrenchValue)).Length + (int)val];
            }
        }

    }

    enum FrenchSeed
    {
        PICCHE,
        QUADRI,
        FIORI,
        CUORI
    }

    enum FrenchValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        OTTO,
        NOVE,
        DIECI,
        JACK,
        REGINA,
        RE
    }
}

