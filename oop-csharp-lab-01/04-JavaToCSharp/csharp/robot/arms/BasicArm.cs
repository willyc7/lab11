﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.csharp.robot.arms
{
    class BasicArm
    {
        private static readonly double ENERGY_REQUIRED_TO_MOVE = 0.2;
        private static readonly double ENERGY_REQUIRED_TO_GRAB = 0.1;
        private bool grabbing;
        private readonly string name;

        public BasicArm(string name)
        {
            this.name = name;
        }

        public bool IsGrabbing()
        {
            return grabbing;
        }
        public void PickUp()
        {
            grabbing = true;
        }

        public void DropDown()
        {
            grabbing = false;
        }

        public double ConsumptionForPickUp
        {
            get { return ENERGY_REQUIRED_TO_MOVE + ENERGY_REQUIRED_TO_GRAB; }
        }

        public double ConsuptionForDropDown
        {
            get { return ENERGY_REQUIRED_TO_MOVE; }
        }

        public override String ToString()
        {
            return name;
        }
    }
}
