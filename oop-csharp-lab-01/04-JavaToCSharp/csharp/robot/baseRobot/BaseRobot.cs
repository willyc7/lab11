﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.csharp.robot.baseRobot
{
    class BaseRobot : IRobot
    {
        public static readonly double BATTERY_FULL = 100;
        public static readonly double MOVEMENT_DELTA_CONSUMPTION = 1.2;
        private static readonly int MOVEMENT_DELTA = 1;

        private double batteryLevel;
        private readonly RobotEnvironment environment;
        private readonly string robotName;

        public double BatteryLevel
        {
            get { return Math.Round(this.batteryLevel * 100d) / BATTERY_FULL; }
            private set { this.batteryLevel = value; }
        }

        public BaseRobot(string robotName)
        {
            this.robotName = robotName;
            this.environment = new RobotEnvironment(new RobotPosition(0,0));
            this.BatteryLevel = BATTERY_FULL;
        }


        public void ConsumeBattery(double amount)
        {
            if (this.BatteryLevel > amount)
            {
                this.BatteryLevel = this.BatteryLevel - amount;
            } else
            {
                this.BatteryLevel = 0;
            }
        }

        private double BatteryForMovement
        {
            get { return MOVEMENT_DELTA; }
        }

        public void ConsumeBatteryForMovement()
        {
            this.ConsumeBattery(this.BatteryForMovement);
        }

        public IPosition2D Position
        {
            get { return this.environment.Position; }
        }

        public bool IsBatteryEnough(double amount)
        {
            return this.BatteryLevel > amount;
        }

        private void Log(string elem)
        {
            Console.WriteLine(elem);
        }

        private bool Move(int dx, int dy)
        {
            if (this.IsBatteryEnough(this.BatteryForMovement))
            {
                if(this.environment.Move(dx, dy))
                {
                    this.ConsumeBatteryForMovement();
                    this.Log("Moved to position " + this.Position.ToString());
                    return true;
                }
                else
                {
                    this.Log("Unable to move");
                }
            } 
            else
            {
                this.Log("Unable to move due to low battery");
            }
            return false;
        }

        public bool MoveDown()
        {
            return this.Move(0, -MOVEMENT_DELTA);
        }

        public bool MoveLeft()
        {
            return this.Move(-MOVEMENT_DELTA, 0);
        }

        public bool MoveRight()
        {
            return this.Move(MOVEMENT_DELTA, 0);
        }

        public bool MoveUp()
        {
            return this.Move(0, MOVEMENT_DELTA);
        }

        public void Recharge()
        {
            this.BatteryLevel = BATTERY_FULL;
        }

        public string ToString()
        {
            return $"Name{this.robotName}";
        }
    }
}
