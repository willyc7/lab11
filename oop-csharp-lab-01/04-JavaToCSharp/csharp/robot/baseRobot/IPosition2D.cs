﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.csharp.robot.baseRobot
{
    public interface IPosition2D
    {
        /**
         * @return X position
         */
        int X
        {
            get;
        }

        /**
         * @return Y position
         */
        int Y
        {
            get;
        }

        /**
         * @param p
         *            delta movement to sum
         * @return the new position
         */
        IPosition2D SumVector(IPosition2D p);

        /**
         * @param x
         *            X delta
         * @param y
         *            Y delta
         * @return the new position
         */
        IPosition2D SumVector(int x, int y);
    }
}
