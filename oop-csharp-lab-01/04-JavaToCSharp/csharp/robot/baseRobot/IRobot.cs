﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.csharp.robot.baseRobot
{

    public interface IRobot
    {
        /**
     * Moves the robot up by one unit
     * 
     * @return true if a movement has been performed
     */
        bool MoveUp();

        /**
         * Moves the robot down by one unit
         * 
         * @return true if a movement has been performed
         */
        bool MoveDown();

        /**
         * Moves the robot left by one unit
         * 
         * @return true if a movement has been performed
         */
        bool MoveLeft();

        /**
         * Moves the robot right by one unit
         * 
         * @return true if a movement has been performed
         */
        bool MoveRight();

        /**
         * Fully recharge the robot
         */
        void Recharge();

        /**
         * @return The robot's current battery level
         */
        double BatteryLevel
        {
            get;
        }

        /**
         * @return The robot environment
         */
        IPosition2D Position
        {
            get;
        }
    }
}
