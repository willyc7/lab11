﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.csharp.robot.baseRobot
{
    public class RobotPosition : IPosition2D
    {
        private readonly int x;
        private readonly int y;

        public RobotPosition(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int X
        {
            get { return this.x; }
        }

        public int Y
        {
            get { return this.y; }
        }

        public IPosition2D SumVector(IPosition2D p)
        {
            return new RobotPosition(p.X + this.x, p.Y + this.y);
        }

        public IPosition2D SumVector(int x, int y)
        {
            return new RobotPosition(x + this.x, y + this.y);
        }

        public override string ToString()
        {
            return $"[{this.x}, {this.y}]";
        }

        public bool Equals(IPosition2D pos)
        {
            return (this.x == pos.X && this.y == pos.Y);
        }
    }
}
